#Question 1
import ballerina/io;
import ballerina/http;

# LearnerProfile
#
# + username - This is the username they'll choose  
# + firstName - This is the learners first name  
# + lastName - This is the learners last name  
# + preferred_formats - The formats they can choose from  
# + past_subjects - An array of subjects.  
public type LearnerProfile record {
    string username;
    string firstName;
    string lastName;
    string[] preferred_formats;
    record {string course; string score;}[] past_subjects;
};

# This is the record for the learning objects
public type LearningObject record {
    MaterialObject[] audio;
    MaterialObject[] video;
    MaterialObject[] text;
};

# This is the record for the Topic Objects
#
# + name - This is the name of the topic  
# + description - This is the description of the topic  
# + difficulty - This is the difficulty of the topic  
public type TopicObject record {
    string name;
    string description;
    string difficulty;
};

# This is the record of the materials
#
# + name - Field Description  
# + description - Field Description  
# + difficulty - Field Description  
public type MaterialObject record {
    string name;
    string description;
    string difficulty;
};

# This is the response object record
#
# + status - Field Description  
# + message - Field Description  
public type ResponseObject record {
    string status;
    string message;
};

# This is the Learning Material
#
# + course - Field Description  
# + learning_objects - Field Description  
public type LearningMaterial record {
    string course;
    LearningObject learning_objects;
};

# This is the request object
#
# + message - Field Description  
public type RequestObject record {
    string message;
};

public function main() {
    io:println("Hello World!");
}
